public class Main {
    public static void main(String[] args) {
        Cofre c = new Cofre();

        c.insertCoin(new Moeda(NomeMoeda.Cinquenta));
        c.insertCoin(new Moeda(NomeMoeda.Cinquenta));
        c.insertCoin(new Moeda(NomeMoeda.UmReal));
        c.insertCoin(new Moeda(NomeMoeda.VinteCinco));
        c.insertCoin(new Moeda(NomeMoeda.Cinco));
        c.insertCoin(new Moeda(NomeMoeda.Cinco));
        c.insertCoin(new Moeda(NomeMoeda.VinteCinco));


        System.out.println(c.coins());
        System.out.println("Reais: "+ c.reais());
        System.out.println("Centavos: " + c.centavos());
        System.out.println(c.coinsType());
    }
}
