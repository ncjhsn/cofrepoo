import java.util.ArrayList;

public class Cofre {
    private Moeda coin;
    private ArrayList<Moeda> vault = new ArrayList<Moeda>();

    public Cofre() {

    }

    public void insertCoin(Moeda m) {
        vault.add(m);
    }

    public String coinsType() {
        String name = "";
        int umR = 0;
        int cinquenta = 0;
        int vintecinco = 0;
        int dez = 0;
        int cinco = 0;
        int um = 0;

        for (Moeda m : vault) {
            if (m.getValorCentavos() == 100) {
                umR++;
            } else if (m.getValorCentavos() == 50) {
                cinquenta++;
            } else if (m.getValorCentavos() == 25) {
                vintecinco++;
            } else if (m.getValorCentavos() == 10) {
                dez++;
            } else if (m.getValorCentavos() == 5) {
                cinco++;
            } else if (m.getValorCentavos() == 1) {
                um++;
            }
        }
        return "[100]: " + umR + "\n" +
               "[50]: " + cinquenta + "\n" +
               "[25]: " + vintecinco + "\n" +
               "[10]: " + dez + "\n" +
               "[5]: " + cinco + "\n" +
               "[1]: " + um + "\n";
    }

    public String coins() {
        String nome = "";
        for (Moeda m : vault) {
            nome += "Valor: " + m.getValorCentavos() + "\n";
        }
        return nome;
    }

    public Double reais() {
        double value = 0;
        for (Moeda m : vault) {
            value += m.getValorReais();
        }
        return value;
    }

    public int centavos() {
        int value = 0;
        for (Moeda m : vault) {
            value += m.getValorCentavos();
        }
        return value;
    }
}
